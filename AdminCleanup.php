<?php
/**
 * Plugin Name:       AdminCleanup
 * Plugin URI:        https://bitbucket.org/lewisdorigo/admincleanup
 * Description:       A simple WordPress plugin that allows hiding or disabling admin functionality based on the environment: development, staging, or production.
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo;

use \Dorigo\Singleton\Singleton;

class AdminCleanup extends Singleton {
    private static $postTypes = [];

    private $menus = [
        'edit-comments.php' => ['production','staging','development'],
        'update-core.php'   => ['production','staging'],
        'plugins.php'       => ['production'],
    ];

    private $subMenus = [
        'themes.php' => [
            'theme-editor.php' => ['production','staging'],
            'customize.php'    => ['production','staging']
        ],
        'index.php' => [
            'update-core.php'  => ['production','staging']
        ],
        'tools.php' => [
            'tools.php'  => ['production','staging'],
            'import.php' => ['production'],
        ],
        'options-general.php' => [
            'options-permalink.php'  => ['production'],
            'options-media.php'      => ['production','staging'],
            'options-discussion.php' => ['production','staging', 'development'],
        ]
    ];

    private $adminBar = [
        'wp-logo' => ['production','staging'],
        'updates' => ['production','staging'],
        'comments' => ['production','staging', 'development'],
    ];

    protected function __construct() {
        $this->env = defined('WP_ENV') ? WP_ENV : 'production';

        add_action('admin_menu', [$this, 'removeMenus'], 9999);
        add_action('wp_before_admin_bar_render', [$this, 'removeAdminBar'], 9999);

        if($this->env !== 'development') {
            add_filter('acf/settings/show_admin', '__return_false');
        }
    }

    public function removeMenus() {
        $menus = apply_filters('Dorigo\AdminCleanup\Menus', $this->menus);
        $subMenus = apply_filters('Dorigo\AdminCleanup\SubMenus', $this->subMenus);

        remove_action('admin_notices', 'update_nag', 3);

        foreach($menus as $menu => $env) {
            if(in_array($this->env, $env)) {
                remove_menu_page($menu);
            }
        }

        foreach($subMenus as $menu => $items) {
            foreach($items as $item => $env) {
                if(in_array($this->env, $env)) {
                    remove_submenu_page($menu, $item);
                }
            }
        }
    }

    public function removeAdminBar() {
        global $wp_admin_bar;

        $menus = apply_filters('Dorigo\AdminCleanup\AdminMenus', $this->adminBar);

        foreach($menus as $menu => $env) {
            if(in_array($this->env, $env)) {
                $wp_admin_bar->remove_menu($menu);
            }
        }
    }
}


AdminCleanup::getInstance();